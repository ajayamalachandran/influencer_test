class PostUpdateType < ActiveRecord::Migration[6.0]
  def change
    rename_column :posts, :type, :kind
    change_column :posts, :kind, :string
    #Ex:- :null => false
    #Ex:- change_column("admin_users", "email", :string, :limit =>25)
    #Ex:- rename_column("admin_users", "pasword","hashed_pasword")
  end
end
