class ChangePosts < ActiveRecord::Migration[6.0]
  def change
    change_table :posts do |t|
      t.change :postUrl, :string ,null: false
      # t.string :description
      # t.integer :commentCount
      # t.string :location
      # t.integer :likeCount
      # t.string :locationId
      t.change :type,:string, null: false
      # t.string :caption
      t.change :profileUrl, :string, null: false
      t.change :userName, :string, null: false
      t.change :fullName, :string, null: false
      t.change :url, :string, null: false
      t.change :postId,:string, null: false,unique: true
      # t.string :query
      # t.timestamps
  end
end
end
