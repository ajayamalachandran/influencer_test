class UserInfluencer < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :influencer_flag, :boolean
  end
end
