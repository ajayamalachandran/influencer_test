class PostChangeCaptionType < ActiveRecord::Migration[6.0]
  def change
    change_column :posts, :caption, :text
    #Ex:- change_column("admin_users", "email", :string, :limit =>25)
  end
end
