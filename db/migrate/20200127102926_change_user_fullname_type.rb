class ChangeUserFullnameType < ActiveRecord::Migration[6.0]
  def change
    change_column_null :posts, :full_name, true
    #Ex:- change_column("admin_users", "email", :string, :limit =>25)
  end
end
