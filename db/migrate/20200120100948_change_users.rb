class ChangeUsers < ActiveRecord::Migration[6.0]
  def change
    change_table :users do |t|
      t.change :username,:string ,null: false,unique: true
      #Ex:- :null => false
      # t.text :description
      # t.string :fullName
      # t.timestamps
  end
end
end
