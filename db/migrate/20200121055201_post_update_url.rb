class PostUpdateUrl < ActiveRecord::Migration[6.0]
  def change
    add_column :posts, :imgUrl, :text
    add_column :posts, :videoUrl, :text
    add_column :posts, :pubDate, :date
  end
end
