class RenameUserSnake < ActiveRecord::Migration[6.0]
  def change
    rename_column :users, :fullName, :full_name
    rename_column :posts, :postUrl, :post_url
    rename_column :posts, :commentCount, :comment_count
    rename_column :posts, :likeCount, :like_count
    rename_column :posts, :locationId, :location_id
    rename_column :posts, :profileUrl, :profile_url
    rename_column :posts, :fullName, :full_name
    rename_column :posts, :imgUrl, :img_url
    rename_column :posts, :videoUrl, :video_url
    rename_column :posts, :pubDate, :pub_date
    rename_column :posts, :viewCount, :view_count
    rename_column :posts, :postId, :post_id
    rename_column :posts, :taggedUsers, :tagged_users

    # taggedUsers

    #Ex:- rename_column("admin_users", "pasword","hashed_pasword")
    #Ex:- rename_column("admin_users", "pasword","hashed_pasword")
  end
end