class CreateBrands < ActiveRecord::Migration[6.0]
  def change
    create_table :brands do |t|
      t.string :username, null: false,unique: true
      t.string :fullName
      t.timestamps
    end
  end
end
