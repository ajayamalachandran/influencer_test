class CreatePosts < ActiveRecord::Migration[6.0]
  def change
    create_table :posts do |t|
      t.string :postUrl
      t.string :description
      t.integer :commentCount
      t.string :location
      t.integer :likeCount
      t.string :locationId
      t.string :type
      t.string :caption
      t.string :profileUrl
      t.string :userName
      t.string :fullName
      t.string :url
      t.string :postId
      t.string :query

      t.timestamps
    end
  end
end
