class AddProcessedToUploads < ActiveRecord::Migration[6.0]
  def change
    add_column :uploads, :processed, :timestamp
  end
end
