class PostUpdateUsername < ActiveRecord::Migration[6.0]
  def change
    rename_column :posts, :userName, :username
    change_column :posts, :username, :string, :null => false
    #Ex:- :null => false
    #Ex:- change_column("admin_users", "email", :string, :limit =>25)
    #Ex:- rename_column("admin_users", "pasword","hashed_pasword") :posts, :column_name, :column_type, :column_options
    #Ex:- change_column("admin_users", "email", :string, :limit =>25)
  end
end
