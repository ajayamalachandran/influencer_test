class UpdatePostsJob < ApplicationJob
  queue_as :default
  sidekiq_options retry: false

  def perform(upload_id)
    upload=Upload.find upload_id
    return if upload.processed.present?
    upload.parse_raw_json_and_update_db
    # Tag Post for Each Brands
    upload.create_update_tagged_brands
    upload.update! processed: Time.now

  end
end
