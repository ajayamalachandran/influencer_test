module Types
    class UserType < BaseObject
      field :id, Integer, null: false
      field :username, String, null: false
      field :full_name, String, null: false
      field :description, String, null: true
      field :posts, PostType.connection_type, null: true
    end
  end