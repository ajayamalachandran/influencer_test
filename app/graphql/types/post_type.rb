module Types

    class PostType < BaseObject     
      field :id, Integer, null: false
      field :post_url, String, null: false
      field :location, String, null: false
      field :description, String, null: true
      field :comment_count, Integer, null: false
      field :like_count, Integer, null: false
      field :kind, String, null: false
      field :caption, String, null: false
      field :profile_url, String, null: false
      field :post_id, Integer, null: false
      field :user_id, Integer, null: false 
      field :view_count, Integer, null: true
    end
  end