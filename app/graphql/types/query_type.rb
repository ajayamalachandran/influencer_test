module Types
  class QueryType < Types::BaseObject

    field :users, [UserType], null: false do
      argument :query, String, required: true
      argument :min_likes_count, Integer, required: false
      argument :min_comments_count, Integer, required: false
      argument :min_views_count, Integer, required: false
    end

    def users(**args)
      count_query = String.new
      search_params = "%#{args[:query]}%"

      if args.has_key?(:min_likes_count)
        count_query << " AND " unless count_query.length == 0
        count_query << "SUM(like_count) >= #{args[:min_likes_count]}"
      end
      if args.has_key?(:min_comments_count)
        count_query << " AND " unless count_query.length == 0
        count_query << "SUM(comment_count) >= #{args[:min_comments_count]}"
      end
      if args.has_key?(:min_views_count)
        count_query << " AND " unless count_query.length == 0
        count_query << "SUM(view_count) >= #{args[:min_views_count]}"
      end

      k = User.joins(:posts).having(count_query).group(:user_id)
      k.where("location LIKE ?",search_params)

    end

    field :posts, Types::PostType.connection_type,null: false do
      argument :query, String, required: true
      argument :min_likes_count, Integer, required: false
      argument :min_comments_count, Integer, required: false
      argument :min_views_count, Integer, required: false
    end

    def posts(**args)
      count_query = String.new
      search_params = "%#{args[:query]}%"
     
      if args.has_key?(:min_likes_count)
        count_query << " AND " unless count_query.length == 0
        count_query << "like_count >= #{args[:min_likes_count]}"
      end
      if args.has_key?(:min_comments_count)
        count_query << " AND " unless count_query.length == 0
        count_query << "comment_count >= #{args[:min_comments_count]}"
      end
      if args.has_key?(:min_views_count)
        count_query << " AND " unless count_query.length == 0
        count_query << "view_count >= #{args[:min_views_count]}"
      end

      Post.where(count_query).where("location LIKE :search_query OR description LIKE :search_query OR caption LIKE :search_query OR username LIKE :search_query OR full_name LIKE :search_query", search_query: search_params)
    end

  end
end
