class Brand < ApplicationRecord
    # belongs_to :post
    # has_one_attached :brand_raw_file
    has_many :tages, class_name: "Tag", foreign_key: "brand_id"
end
