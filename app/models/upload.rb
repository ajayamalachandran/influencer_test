class Upload < ApplicationRecord
    has_one_attached :raw_file
    validate :correct_document_mime_type

    def parse_raw_json_and_update_db
    Utils.import_json(raw_file.download)
    end
    

    # Brands And Posts in Tag table
    def create_update_tagged_brands
        Utils.create_brand_tag(raw_file.download)
    end

    def schedule_parse
        UpdatePostsJob.perform_later  id
    end


    private
    def correct_document_mime_type
      if raw_file.attached? && !raw_file.content_type.in?(%w(application/json))
        raw_file.purge # delete the uploaded file
        errors.add(:raw_file, 'Please Upload a json Document')
      end
    end
end
