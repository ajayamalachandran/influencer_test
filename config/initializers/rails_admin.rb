Dir[Rails.root.join('lib', 'rails_admin', '*.rb')].each {|file| require file }
RailsAdmin.config do |config|

  ### Popular gems integration

  ## == Devise ==
  config.authenticate_with do
    authenticate_or_request_with_http_basic('Login required') do |name, password|
      (name == Rails.application.credentials.rails_admin[:username] && password == Rails.application.credentials.rails_admin[:password])
    end
  end
  # config.current_user_method(&:current_user)

  ## == CancanCan ==
  # config.authorize_with :cancancan

  ## == Pundit ==
  # config.authorize_with :pundit

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  ## == Gravatar integration ==
  ## To disable Gravatar integration in Navigation Bar set to false
  # config.show_gravatar = true

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app
    parse_raw_json_and_update


    ## With an audit adapter, you can add:
    # history_index
    # history_show
  end
  config.model 'Upload' do 
    edit do
      configure :processed do 
      visible false
      # hide
      end
    end
  end
end
