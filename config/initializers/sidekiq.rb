require 'sidekiq'
require 'sidekiq/web'
Sidekiq::Extensions.enable_delay!
Sidekiq::Web.use(Rack::Auth::Basic) do |user, password|
  [user, password] == [Rails.application.credentials.rails_admin[:username], Rails.application.credentials.rails_admin[:password]]
end
Sidekiq.configure_client do |config|
  url = Rails.application.credentials.aws[:redis_url]
  config.redis = { :size => 1, url: url }
end
Sidekiq.configure_server do |config|
  config.server_middleware do |chain|
    url = Rails.application.credentials.aws[:redis_url]
    config.redis = { :size => 4, url: url }
  end
end