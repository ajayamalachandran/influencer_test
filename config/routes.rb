Rails.application.routes.draw do
  post "/graphql", to: "graphql#execute"
  mount RailsAdmin::Engine => '/chamber-of-secrets', as: 'rails_admin'
  mount Sidekiq::Web, at: '/busy-bird-at-work'
  get 'welcome/index'
  post 'post/create'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  # root 'welcome#index'
end
