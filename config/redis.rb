module Influencers
    def self.redis
      @redis ||= Redis.new(url: Rails.application.credentials.aws[:redis_url])
    end
  end