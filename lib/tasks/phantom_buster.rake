namespace :phantom_buster do
  desc "IMPORT"
  task import_json: :environment do
    puts "data/#{ENV['file']}"
    raw_json=File.read("data/#{ENV['file']}");
    Utils.import_json(raw_json)
  end

end
