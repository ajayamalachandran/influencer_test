class Utils
    def self.import_json(raw_json)
        posts=[]
        json=JSON.parse(raw_json)
        unique_user_params= {}
        json = json.map do |json_raw| 
            json_raw['postId'].present? ? json_raw : json_raw.merge({ "postId" => json_raw['id']})
        end
        json.each do |raw_obj|
            unless unique_user_params[raw_obj['username']].present?
                unique_user_params[raw_obj['username']] = raw_obj.slice('username','fullName')
            end
        end
        unique_user_names = unique_user_params.keys
        user_names_not_in_db= unique_user_names - User.where(username: unique_user_names).pluck(:username)
            user_params_not_in_db = unique_user_params.select{|k,v| user_names_not_in_db.include? k }
            users_builder = user_params_not_in_db.map do |key,user_params|
                User.new(user_params.transform_keys{ |key| key.to_s.underscore })
            end
        User.import users_builder
        unique_post_params=json.map { |k|  k['postId']}
        users_from_db_matched_raw = User.where(username: unique_user_names).select(:username,:id).as_json.reduce({}, &-> (acc, i) { acc.merge ({i['username'] => i['id']}) })
        posts_not_in_db= unique_post_params - Post.where(post_id: unique_post_params).pluck(:post_id)
        post_params_builder = json.map do |raw_obj|
            if posts_not_in_db.present? && posts_not_in_db.include?(raw_obj['postId'])
                post_params = raw_obj.except('isSidecar','timestamp','instagramPostUrl')
                post_params['kind']=raw_obj['type']
                post_params = post_params.except('type')
                raw_username = post_params['username']
                post_params['user_id'] = users_from_db_matched_raw[raw_username]
                post_params = post_params.reject{|k,v|  k.include?('taggedUsername') || k.include?('taggedFullName') }
                post_params['tagged_users'] = raw_obj.select{|k,v| k =~ /taggedUsername/}.values
                Post.new post_params.transform_keys{ |key| key.to_s.underscore }
            else
                update_post_params = raw_obj.except('isSidecar','timestamp','instagramPostUrl')
                update_post_params['kind']=raw_obj['type']
                update_post_params = update_post_params.except('type')
                raw_username = update_post_params['username']
                update_post_params['user_id'] = users_from_db_matched_raw[raw_username]
                update_post_params = update_post_params.reject{|k,v|  k.include?('taggedUsername') || k.include?('taggedFullName') }
                update_post_params['tagged_users'] = raw_obj.select{|k,v| k =~ /taggedUsername/}.values
                final_post_params=Post.new update_post_params.transform_keys{ |key| key.to_s.underscore }
                Post.where(post_id: update_post_params['post_id']).each do |q|
                    q.update(update_post_params.transform_keys{ |key| key.to_s.underscore })
                end
                nil
            end
        end
        Post.import post_params_builder.compact
          puts "Total number of Records in  users=#{User.count} && posts=#{Post.count}"
    end

    # Tagged Post and Brand
    def self.create_brand_tag(raw_json)
        tmp_arr=[];
        json=JSON.parse(raw_json)
        json = json.map do |json_raw| 
            json_raw['postId'].present? ? json_raw : json_raw.merge({ "postId" => json_raw['id']})
        end
        tagged_brand_params_builder=json.select { |raw_json| raw_json.has_key? "taggedUsername1" }
        tagged_brand_params_builder=tagged_brand_params_builder.map {|raw_json| { post_id: raw_json["postId"],tagged_users: raw_json.select{|k,v| k =~ /taggedUsername/}.values } }
                tagged_users_array=tagged_brand_params_builder.map { |tagged_users| tagged_users[:tagged_users] }.flatten.uniq
                users_not_from_db= tagged_users_array - Brand.where(username: tagged_users_array).pluck(:username)
                if users_not_from_db.present?
                    users_params =users_not_from_db.map { |username| {:username => username} }
                    Brand.import users_params
                end
                post_id_array=tagged_brand_params_builder.map { |post_json|  post_json[:post_id] } #Gets post_ids from the uploaded file
                tmp_arr=[]
                tag_params_builder=[]
                brand_tag=Brand.where(username: tagged_users_array).pluck(:username,:id).map { |brand_| {:username => brand_[0],:id => brand_[1]}}
                post_tag=Post.where(post_id: post_id_array).pluck(:id,:tagged_users,:user_id).map {|post_| {:id => post_[0] ,:tagged_users => post_[1],:user_id => post_[2]}}

                brand_tag.each do |brand_json|
                    post_tag.each do |post_json|
                        if post_json[:tagged_users].include? brand_json[:username]
                            if tmp_arr.exclude? brand_json[:username]
                                tmp_arr.push(brand_json[:username])
                                tag_params_builder.push( Tag.new({:brand_id => brand_json[:id],:post_id => post_json[:id] ,:user_id => post_json[:user_id]}))
                            end
                        end
                    end
                end
                Tag.import tag_params_builder
         end
end


  # tagged_brand_params_builder=json.each do |raw_json| 
        #     raw_json.each do |k,v|
        #         if k.include?('taggedUsername')
        #             if tmp_arr.exclude?(raw_json['postId'])
        #                 tmp_arr.push(raw_json['postId'])
        #                 tagged_json_array.push( { post_id:raw_json['postId'],tagged_users: raw_json.select{|k,v| k =~ /taggedUsername/}.values })
        #             end
        #         end
        #      end
        #     end