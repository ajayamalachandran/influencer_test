module RailsAdmin
    module Config
      module Actions
        class ParseRawJsonAndUpdate < RailsAdmin::Config::Actions::Base
          RailsAdmin::Config::Actions.register(self)
          register_instance_option :pjax? do
            false
          end 
          register_instance_option :visible? do
            # return true 
            bindings[:object].class == Upload && !bindings[:object].processed.present?
          end
          register_instance_option :member do
            true
          end
          register_instance_option :link_icon do
            'icon-cloud-download'
          end
          register_instance_option :controller do
            Proc.new do
              @object.schedule_parse
              unless @object.errors.present?
                flash[:success] = "Process running on the background"
              else
                flash[:notice] = "Unable to Process Json File."
              end
              redirect_to back_or_index
            end
          end
        end
      end
    end
  end
  # Succesfully Users#{User.count} - Posts- #{Post.count}